﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using nav_service.Models;
using Newtonsoft.Json;

namespace nav_service.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoansController : ApiController
    {
        private nav_serviceContext db = new nav_serviceContext();
        LoanOperator controller = new LoanOperator();

        // GET: api/Loans
        [Route("api/v1/loans/")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetLoans()
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnAllMembersLoans();
                var data = dataResult;

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }

        // GET: api/Loans/5
        [ResponseType(typeof(Loan))]
        public async Task<HttpResponseMessage> GetLoan(string id)
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnLoan(id);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }

        // PUT: api/Loans/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLoan(string id, Loan loan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != loan.LoanNo)
            {
                return BadRequest();
            }

            db.Entry(loan).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Loans
        [ResponseType(typeof(Loan))]
        public async Task<IHttpActionResult> PostLoan(Loan loan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Loans.Add(loan);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LoanExists(loan.LoanNo))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = loan.LoanNo }, loan);
        }

        // DELETE: api/Loans/5
        [ResponseType(typeof(Loan))]
        public async Task<IHttpActionResult> DeleteLoan(string id)
        {
            Loan loan = await db.Loans.FindAsync(id);
            if (loan == null)
            {
                return NotFound();
            }

            db.Loans.Remove(loan);
            await db.SaveChangesAsync();

            return Ok(loan);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LoanExists(string id)
        {
            var dataResult = controller.ReturnMemberLoans(id);
            if (string.IsNullOrEmpty(dataResult))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}