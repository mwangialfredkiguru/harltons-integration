﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using nav_service.Models;

namespace nav_service.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MembersController : ApiController
    {
        private nav_serviceContext db = new nav_serviceContext();
        MemberNAVOperator controller = new MemberNAVOperator();
        // GET: api/Members
        [Route("api/v1/members/")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetMembers()
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnAllMembers();

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }
        // GET: api/Members/5
        [ResponseType(typeof(Member))]
        public async Task<HttpResponseMessage> GetMember(string id)
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnMember(id);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }

        // PUT: api/Members/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMember(string id, Member member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != member.No)
            {
                return BadRequest();
            }

            db.Entry(member).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MemberExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Members
        [ResponseType(typeof(Member))]
        public async Task<IHttpActionResult> PostMember(Member member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Members.Add(member);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MemberExists(member.No))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = member.No }, member);
        }

        // DELETE: api/Members/5
        [ResponseType(typeof(Member))]
        public async Task<IHttpActionResult> DeleteMember(string id)
        {
            Member member = await db.Members.FindAsync(id);
            if (member == null)
            {
                return NotFound();
            }

            db.Members.Remove(member);
            await db.SaveChangesAsync();

            return Ok(member);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MemberExists(string id)
        {
            var dataResult = controller.ReturnMember(id);
            if (string.IsNullOrEmpty(id))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}