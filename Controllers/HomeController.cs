﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace nav_service.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HomeController : ApiController
    {
        string str = "API for sacco intergration";
        public async Task<HttpResponseMessage> GetHome()
        {            
            return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(str) };
        }

        public async Task<HttpResponseMessage> GetHome(string id)
        {
            return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(str) };
        }
    }
}