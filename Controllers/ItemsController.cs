﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using nav_service.Models;

namespace nav_service.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ItemsController : ApiController
    {
        private nav_serviceContext db = new nav_serviceContext();
        ItemNAVOperator controller = new ItemNAVOperator();

        [Route("api/v1/items/")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetItems()
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnAllItems();

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again. "+ ex.Message) };
            }
        }

        [ResponseType(typeof(Items))]
        public async Task<HttpResponseMessage> GetItem(string id)
        {

            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnItem(id);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutItems(string id, Items Items)
        {
            return StatusCode(HttpStatusCode.NoContent);
        }
        [Route("api/v1/items/")]
        [HttpPost]
        public async Task<HttpResponseMessage> PostItems(Items item)
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.PostItem(item);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }
        private bool ItemsExists(string id)
        {
            return false;
        }
    }
}