﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using nav_service.Models;

namespace nav_service.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SaleController : ApiController
    {
        private nav_serviceContext db = new nav_serviceContext();
        SalesNAVOperator controller = new SalesNAVOperator();

        [Route("api/v1/saleinvoices/")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetSaleInvoices()
        {
            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnAll();

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again. "+ ex.Message) };
            }
        }

        // GET: api/Banks/5
        [ResponseType(typeof(Bank))]
        public async Task<HttpResponseMessage> GetSaleInvoice(string id)
        {

            try
            {
                var encodedUsernamePassword = "T25ldGlsbCZTYWNjbw==";// Request.Headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(encodedUsernamePassword))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the API credentials or is empty.") };
                }
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var cred = UtilityClass.Decrypt(UtilityClass.AuthCode);
                if (usernamePassword != cred)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please pass the correct API credentials or is empty.") };
                }
                var dataResult = controller.ReturnOne(id);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(dataResult + "")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Please check your request and try again.") };
            }
        }

        // PUT: api/Banks/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBank(string id, Bank bank)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bank.Id)
            {
                return BadRequest();
            }

            db.Entry(bank).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BankExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Banks
        [ResponseType(typeof(Bank))]
        public async Task<IHttpActionResult> PostBank(Bank bank)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Banks.Add(bank);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BankExists(bank.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = bank.Id }, bank);
        }

        // DELETE: api/Banks/5
        [ResponseType(typeof(Bank))]
        public async Task<IHttpActionResult> DeleteBank(string id)
        {
            Bank bank = await db.Banks.FindAsync(id);
            if (bank == null)
            {
                return NotFound();
            }

            db.Banks.Remove(bank);
            await db.SaveChangesAsync();

            return Ok(bank);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BankExists(string id)
        {
            return db.Banks.Count(e => e.Id == id) > 0;
        }
    }
}