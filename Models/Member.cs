﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nav_service.Models
{
    public class Member
    {
        [Key]
        public string No { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phonenumber { get; set; }
        public string county { get; set; }
    }
}