﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nav_service.Models
{
    public class Items
    {
        [Key]
        public string number { get; set; }
        public string displayName { get; set; }
        public string type { get; set; }
        public string itemCategoryId { get; set; }
        public string itemCategoryCode { get; set; }        
        public bool blocked { get; set; }
        public string baseUnitOfMeasureId { get; set; }
        public string gtin { get; set; }
        public int inventory { get; set; }
        public decimal unitPrice { get; set; }
        public bool priceIncludesTax { get; set; }
        public decimal unitCost { get; set; }
        public string taxGroupId { get; set; }
        public string taxGroupCode { get; set; }
        public baseUnitOfMeasure baseUnitOfMeasure;
    }
    public class baseUnitOfMeasure
    {
        public string code { get; set; }
        public string displayName { get; set; }
        public string symbol { get; set; }
        public string unitConversion { get; set; }
    }
}