﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nav_service.Models
{
    public class Loan
    {
        [Key]
        public string LoanNo { get; set; }
        public string MemberNo { get; set; }
        public string MemberName { get; set; }
        public string RequestedAmount { get; set; }
        public string ApprovedAmount { get; set; }
        public string DisbursedAmount { get; set; }
        public string InterestRate { get; set; }
        public string Installements { get; set; }
        public string Posted { get; set; }
        public string PostedDate { get; set; }
    }
}