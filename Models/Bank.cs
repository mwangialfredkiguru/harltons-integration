﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nav_service.Models
{
    public class Bank
    {
        [Key]
        public string Id { get; set; }
        public string fullname { get; set; }
        public string bankbranchno { get; set; }
        public string bankaccno { get; set; }
        public string balancelcy { get; set; }
        public string debitamountlcy { get; set; }
        public string creditamountlcy { get; set; }
    }
}