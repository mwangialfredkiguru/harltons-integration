﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

public class nav_serviceContext : DbContext
{
    // You can add custom code to this file. Changes will not be overwritten.
    // 
    // If you want Entity Framework to drop and regenerate your database
    // automatically whenever you change your model schema, please use data migrations.
    // For more information refer to the documentation:
    // http://msdn.microsoft.com/en-us/data/jj591621.aspx

    public nav_serviceContext() : base("name=nav_serviceContext")
    {
    }

    public System.Data.Entity.DbSet<nav_service.Models.Member> Members { get; set; }

    public System.Data.Entity.DbSet<nav_service.Models.Loan> Loans { get; set; }

    public System.Data.Entity.DbSet<nav_service.Models.Bank> Banks { get; set; }
}
