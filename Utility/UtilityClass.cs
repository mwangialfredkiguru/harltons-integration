﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace nav_service.Controllers
{
    public static class UtilityClass
    {
        #region Initialize variables
        static string DynamicsNAVServer = ConfigurationManager.AppSettings["DYNAMICSSERVER"];// "localhost";
        static string str = ConfigurationManager.AppSettings["PORT"].ToString();
        static String Port = Decrypt(str);// "r4DZwcJYGbNqvQK1Jch7a2dslQJBHWrWJGvfRSyHUE0L9uslVpwzvvVtevdwhHdVzxcqzApQZcSDmHnroYTtkdRNjq1ntcTvz1MBXh85v3pR/uWRPsZoVI4CkvjoffsS";
        static String Instance = Decrypt(ConfigurationManager.AppSettings["INSTANCE"]);//"nNIy6flen2/meswy8NpaT+ox/76lsWldntGqGUvAmL1g3yi11WKMLfKqOSzkewKUI10ihWeDbggBvYyLWQsa5irlxMqfQuSJWl++tXuh0yeHvXYucj7QVd4kWDlMR5At";
        static public String Domain = "";
        static public String Company = Decrypt(ConfigurationManager.AppSettings["NAVCOMPANY"]);//"WBp2rgZ2RLuoWemJEKdy9nG5QxEMurI8+2M63gNAHr+XbVyywl7mE7wOrZYvZE6sfiJJWUxRk+SYIeHOjWYzod9jVNmdWRNNnQxasGgM/RdKStH8hDvXsU1fzy8aCflg";
        static public String PortalUser = "";
        static public String PortalPass = "";

        public static String NAVServicename = "PortalIntergrationService";
        public static String NAVLoanServicename = "MemberLoansService";

        public static String NavServicePageURL = "http://" + DynamicsNAVServer + ":" + Port + "/" + Instance + "/WS/" + Company + "/Page/";
        public static String NavServiceCodeUnitURL = "http://" + DynamicsNAVServer + ":" + Port + "/" + Instance + "/WS/" + Company + "/Codeunit/";


        public static string AuthCode = "QaA8yvFxS/qFVLQyn8waZHxEhsrd8gDEOPzsgKh/F9MN+GqgJ9A1Qm38qhh1aGLoj0iq+qgPuZfZ8EUy+bgWkdOgwUxj32zOGUECHKgKWosXW3KKTkEruNibOOYMVvlr";
        
        private const int Keysize = 256;
        private const int DerivationIterations = 1000;
        public static String Raw_Company = ConfigurationManager.AppSettings["COMPANY"];
        static string passPhraseCompany = "Onetill&Sacco";

        public static string USERNAME = "Administrator";
        public static string PASSWORD = "CT2gNmwB@DP2z;AZE!8Q*@eNI4?2aF)j";
        public static string DOMAIN = "";

        public static String BASEURL1 = "http://localhost:8048/apiservice/api/beta/companies(d90e057f-53d0-4fb7-8b80-e72b5d45d948)/items";
        public static String BASEURL = "http://localhost:8048/apiservice/api/beta/companies";
        public static string COMPANYID = "d90e057f-53d0-4fb7-8b80-e72b5d45d948";

        #endregion
        #region Extra Code | Decrypt Data | Logger
        public static string Decrypt(string cipherText)
        {
            try
            {
                if (String.IsNullOrEmpty(cipherText))
                {
                    return "";
                }
                string passPhrase = "Onetill&Sacco";// passPhraseCompany;
                var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
                var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
                var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
                var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

                using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                {
                    var keyBytes = password.GetBytes(Keysize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.BlockSize = 256;
                        symmetricKey.Mode = CipherMode.CBC;
                        symmetricKey.Padding = PaddingMode.PKCS7;
                        using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                        {
                            using (var memoryStream = new MemoryStream(cipherTextBytes))
                            {
                                using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    var plainTextBytes = new byte[cipherTextBytes.Length];
                                    var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    memoryStream.Close();
                                    cryptoStream.Close();
                                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        /*Function to convert dates*/
        public static string ConvertDates(String APIDate)
        {
            string dateInfo = null;
            try
            {
                dateInfo = Convert.ToDateTime(APIDate).ToString("MM/dd/yyyy");// + "D";
            }
            catch (Exception ex)
            {

            }
            return dateInfo;
        }

        public static void WriteToLog(string title, string msg, string msgdetails = null, string foldername1 = null, string filename1 = null)
        {
            string BASEDIRECTORY = @"C:\SaccoLogs\";
            string logDate = DateTime.Now.ToString("dd-MM-yyyy");
            string foldername = string.IsNullOrEmpty(foldername1) ? "SaccoLogs" : foldername1;
            string filename = string.IsNullOrEmpty(filename1) ? "SaccoLogs" : filename1;
            try
            {
                String DirectoryLocation = BASEDIRECTORY + foldername + "-" + logDate;
                String DirectoryLocationFile = @DirectoryLocation + "\\" + filename + "-" + logDate + ".txt";
                if (Directory.Exists(DirectoryLocation) != true)
                {
                    Directory.CreateDirectory(DirectoryLocation);
                }
                //'check the file 
                FileStream fs = new FileStream(DirectoryLocationFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamWriter s = new StreamWriter(fs);
                s.Close();
                fs.Close();
                FileStream fs1 = new FileStream(DirectoryLocationFile, FileMode.Append, FileAccess.Write);
                StreamWriter s1 = new StreamWriter(fs1);
                s1.Write("Title: " + title + Environment.NewLine);
                s1.Write("Message: " + msg + Environment.NewLine);
                s1.Write("Message Details: " + msgdetails + Environment.NewLine);
                s1.Write("Date/Time: " + DateTime.Now.ToString() + Environment.NewLine);
                s1.Write("===========================================================================================" + Environment.NewLine);
                s1.Close();
                fs1.Close();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}