﻿using nav_service.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace nav_service.Controllers
{
    public class ItemNAVOperator
    {
        static String BASEURL = UtilityClass.BASEURL;
        static string COMPANYID = UtilityClass.COMPANYID;

        string USERNAME = UtilityClass.USERNAME;
        string PASSWORD = UtilityClass.PASSWORD;
        string DOMAIN = UtilityClass.DOMAIN;

        static string RESCOURCE = "/items/";

        string URL_status = BASEURL + "(" + COMPANYID + ")" + RESCOURCE;
        public object ReturnAllItems()
        {
            object data = null;
            try
            {
                CredentialCache myCache = new CredentialCache();
                myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                MyWebClient webClient = new MyWebClient();
                webClient.Credentials = myCache;
                string result = webClient.DownloadString(new Uri(URL_status));
                var data1 = JsonConvert.DeserializeObject(result);
                data = data1;
            }
            catch (Exception ex)
            {                
                UtilityClass.WriteToLog("ReturnAllItems", ex.Message, ex.InnerException + "");
                var message = ex.Message;
                data = JsonConvert.DeserializeObject(message);
            }
            return data;
        }
        public object ReturnItem(string MemberNo)
        {
            object data = null;
            if (!string.IsNullOrEmpty(MemberNo))
            {
                try
                {
                    CredentialCache myCache = new CredentialCache();
                    myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                    MyWebClient webClient = new MyWebClient();
                    webClient.Credentials = myCache;
                    string result = webClient.DownloadString(new Uri(URL_status));
                    var data1 = JsonConvert.DeserializeObject(result);
                    data = data1;
                }
                catch (Exception ex)
                {
                    UtilityClass.WriteToLog("ReturnItem " + MemberNo, ex.Message, ex.InnerException + "");
                    var message = ex.Message;
                    data = JsonConvert.DeserializeObject(message);
                }
            }
            return data;
        }

        public object PostItem(Items itemdata)
        {
            object data1 = null;
            try
            {
                CredentialCache myCache = new CredentialCache();
                myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                MyWebClient webClient = new MyWebClient();
                webClient.Credentials = myCache;
                var data = JsonConvert.SerializeObject(itemdata);
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json; charset=utf-8");
                var bytedata = Encoding.UTF8.GetBytes(data);
                var byteArray = webClient.UploadData(URL_status, "POST", bytedata);
                string result = System.Text.Encoding.UTF8.GetString(byteArray);
                data1 = JsonConvert.DeserializeObject(result);
            }
            catch (Exception ex)
            {
                UtilityClass.WriteToLog("PostItem ", ex.Message, ex.InnerException + "");
                var message = ex.Message;
                data1 = JsonConvert.DeserializeObject(message);
            }
            return data1;
        }
    }
}