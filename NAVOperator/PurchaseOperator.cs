﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace nav_service.Controllers
{
    public class PurchaseNAVOperator
    {
        String BASEURL = UtilityClass.BASEURL;
        string COMPANYID = UtilityClass.COMPANYID;

        string USERNAME = UtilityClass.USERNAME;
        string PASSWORD = UtilityClass.PASSWORD;
        string DOMAIN = UtilityClass.DOMAIN;

        string RESCOURCE = "/purchaseInvoices/";
        public object ReturnAll()
        {
            object data = null;
            try
            {
                string URL_status = BASEURL+"("+COMPANYID+")" + RESCOURCE;

                CredentialCache myCache = new CredentialCache();
                myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                MyWebClient webClient = new MyWebClient();
                webClient.Credentials = myCache;
                string result = webClient.DownloadString(new Uri(URL_status));
                var data1 = JsonConvert.DeserializeObject(result);
                data = data1;
            }
            catch (Exception ex)
            {
                UtilityClass.WriteToLog("ReturnAllItems", ex.Message, ex.InnerException + "");
            }
            return data;
        }
        public object ReturnOne(string MemberNo)
        {
            object data = null;
            if (!string.IsNullOrEmpty(MemberNo))
            {
                try
                {
                    string URL_status = BASEURL + "(" + COMPANYID + ")" + RESCOURCE + MemberNo;

                    CredentialCache myCache = new CredentialCache();
                    myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                    MyWebClient webClient = new MyWebClient();
                    webClient.Credentials = myCache;
                    string result = webClient.DownloadString(new Uri(URL_status));
                    var data1 = JsonConvert.DeserializeObject(result);
                    data = data1;
                }
                catch (Exception ex)
                {
                    UtilityClass.WriteToLog("ReturnItem " + MemberNo, ex.Message, ex.InnerException + "");
                }
            }
            return data;
        }
    }
}