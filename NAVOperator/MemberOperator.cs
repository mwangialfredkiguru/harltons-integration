﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using nav_service.PortalIntergrationService_Reference;
using Newtonsoft.Json;

namespace nav_service.Controllers
{
    public class MemberNAVOperator
    {
        string NavServiceCodeUnitURL = UtilityClass.NavServiceCodeUnitURL;
        string NAVServicename = UtilityClass.NAVServicename;
        String BASEURL = "http://localhost:1148/BC130/api/beta/companies";
        string COMPANYID = "f40358a1-dd48-4049-a0e4-de233312d153";

        string USERNAME = UtilityClass.USERNAME;// "kwambua";//""mwangi";
        string PASSWORD = UtilityClass.PASSWORD;// "@High&secure_2021#P@$$w0rd";//"B@B@Diane";
        string DOMAIN = UtilityClass.DOMAIN;//"LOCIAFRICA";
        public object ReturnAllMembers()
        {
            object data = null;
            try
            {
                string URL_status = BASEURL+"("+COMPANYID+")" +"/members/";

                CredentialCache myCache = new CredentialCache();
                myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                MyWebClient webClient = new MyWebClient();
                webClient.Credentials = myCache;
                string result = webClient.DownloadString(new Uri(URL_status));
                var data1 = JsonConvert.DeserializeObject(result);
                data = data1;
            }
            catch (Exception ex)
            {
                UtilityClass.WriteToLog("ReturnAllMembers", ex.Message, ex.InnerException + "");
            }
            return data;
        }
        public object ReturnMember(string MemberNo)
        {
            object data = null;
            if (!string.IsNullOrEmpty(MemberNo))
            {
                try
                {
                    string URL_status = BASEURL + "(" + COMPANYID + ")" + "/members/"+ MemberNo;

                    CredentialCache myCache = new CredentialCache();
                    myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                    MyWebClient webClient = new MyWebClient();
                    webClient.Credentials = myCache;
                    string result = webClient.DownloadString(new Uri(URL_status));
                    var data1 = JsonConvert.DeserializeObject(result);
                    data = data1;
                }
                catch (Exception ex)
                {
                    UtilityClass.WriteToLog("ReturnMember "+ MemberNo, ex.Message, ex.InnerException + "");
                }
            }
            return data;
        }
    }
    public class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            if (request is HttpWebRequest)
            {
                var myWebRequest = request as HttpWebRequest;
                myWebRequest.UnsafeAuthenticatedConnectionSharing = true;
                myWebRequest.KeepAlive = true;
            }

            return request;
        }
    }
}