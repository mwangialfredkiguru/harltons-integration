﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using nav_service.PortalIntergrationService_Reference;
using Newtonsoft.Json;

namespace nav_service.Controllers
{
    public class LoanOperator
    {
        string NavServicePageURL = UtilityClass.NavServicePageURL;
        string NavServiceCodeUnitURL = UtilityClass.NavServiceCodeUnitURL;
        string NAVLoanServicename = UtilityClass.NAVLoanServicename;
        string NAVServicename = UtilityClass.NAVServicename;

        String BASEURL = "http://localhost:1148/BC130/api/beta/companies";
        string COMPANYID = "f40358a1-dd48-4049-a0e4-de233312d153";
        String APIENDPOINT = "/memberloanlist/";
        string USERNAME = UtilityClass.USERNAME;// "kwambua";//""mwangi";
        string PASSWORD = UtilityClass.PASSWORD;// "@High&secure_2021#P@$$w0rd";//"B@B@Diane";
        string DOMAIN = UtilityClass.DOMAIN;//"LOCIAFRICA";

        public object ReturnAllMembersLoans()
        {
            object data = null;
            try
            {
                string URL_status = BASEURL + "(" + COMPANYID + ")" + APIENDPOINT;

                CredentialCache myCache = new CredentialCache();
                myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, DOMAIN));

                MyWebClient webClient = new MyWebClient();
                webClient.Credentials = myCache;
                string result = webClient.DownloadString(new Uri(URL_status));
                var data1 = JsonConvert.DeserializeObject(result);
                data = data1;
            }
            catch (Exception ex)
            {
                UtilityClass.WriteToLog("ReturnAllMembersLoans", ex.Message, ex.InnerException + "");
            }
            return data;
        }
        public object ReturnLoan(string no)
        {
            object data = null;
            if (!string.IsNullOrEmpty(no))
            {
                try
                {
                    string URL_status = BASEURL + "(" + COMPANYID + ")" + APIENDPOINT + no;

                    CredentialCache myCache = new CredentialCache();
                    myCache.Add(new Uri(URL_status), "NTLM", new NetworkCredential(USERNAME, PASSWORD, ""));

                    MyWebClient webClient = new MyWebClient();
                    webClient.Credentials = myCache;
                    string result = webClient.DownloadString(new Uri(URL_status));
                    var data1 = JsonConvert.DeserializeObject(result);
                    data = data1;
                }
                catch (Exception ex)
                {
                    UtilityClass.WriteToLog("ReturnLoan " + no, ex.Message, ex.InnerException + "");
                }
            }
            return data;
        }
        public string ReturnMemberLoans(string MemberNo)
        {
            string data = null;
            if (!string.IsNullOrEmpty(MemberNo))
            {
                try
                {
                    PortalIntergrationService Intergrate = new PortalIntergrationService();
                    Intergrate.UseDefaultCredentials = true;
                    String compUrl = NavServiceCodeUnitURL + NAVServicename;
                    String httpCompUrl = compUrl.Replace(" ", "%20");
                    Intergrate.Url = httpCompUrl;
                    data = Intergrate.ReturnMemberLoans(MemberNo);
                }
                catch (Exception ex)
                {
                    UtilityClass.WriteToLog("ReturnMember "+ MemberNo, ex.Message, ex.InnerException + "");
                }
            }
            return data;
        }
    }
}